//
//  ViewController.swift
//  GameGR1
//
//  Created by Sebastian Guerrero on 11/14/17.
//  Copyright © 2017 SG. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  
  //MARK:- Attributes
  
  let gameModel = GameModel()
  
  //MARK:- Outlets
  
  @IBOutlet weak var objetivoLabel: UILabel!
  @IBOutlet weak var puntajeLabel: UILabel!
  @IBOutlet weak var rondaLabel: UILabel!
  @IBOutlet weak var slider: UISlider!
  
  //MARK:- ViewController LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    setValores()
  }
  
  //MARK:- Actions

  @IBAction func jugarButtonPressed(_ sender: Any) {
    gameModel.jugar(valorIntento: Int(round(slider.value)))
    setValores()
  }
  
  @IBAction func reiniciarButtonPressed(_ sender: Any) {
    gameModel.reiniciar()
    setValores()
  }
  
  func setValores(){
    objetivoLabel.text = "\(gameModel.objetivo ?? 0)"
    puntajeLabel.text = "\(gameModel.puntaje)"
    rondaLabel.text = "\(gameModel.ronda)"
  }
}

